import { Component, OnInit } from '@angular/core';        /* Импорт декоратора для компоненты */
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({                  /* Декоратор для компоненты */
  selector: 'app-games-picker',      /* Название селектора для вставки компонента в HTML код */
  templateUrl: './app.template.pug',       /* HTML шаблон для компоненты */
  styleUrls: ['./app.style.styl']     /* Стили для компоненты */
})

export class AppComponent implements OnInit {
  constructor(private http: Http) {
  }
  title = 'Games Store';   /* Свойство которое можно использовать в компоненте */
  games =  [];
  picked = [];
  turner: boolean = false;
  newGames;
  adder(game: any) {
    game.added = !game.added;
    console.log(game);
    let arr = [];
    for (let i = 0; i < this.games.length; i++) {
      if (this.games[i].added) {
        arr.push(this.games[i]);
        this.picked = arr;
      }
    }
    if (arr.length !== 0) {
      localStorage.setItem('storage', JSON.stringify(arr));
    }
    if (arr.length === 0) {
      this.picked = arr;
      localStorage.clear();
    }
  }
  toMyItems() {
    this.turner = true;
  }
  toStore() {
    this.turner = false;
  }
  clearMyList() {
    for (let n = 0; n < this.games.length; n++) {
      this.games[n].added = false;
    }
    this.picked = [];
    localStorage.clear();
  }
  ngOnInit() {
    this.newGames = this.getGames().then((data) => {
      let arr = [];
      for (let key in data) {
        arr.push(data[key]);
      }
      this.games = arr;
      if (localStorage.getItem('storage')) {
        let arr = JSON.parse(localStorage.getItem('storage')); // Парсим локалсторейдж
        this.picked = arr;
        for (let i = 0; i < this.games.length; i++) {
          for (let j = 0; j < arr.length; j++) {
            if (this.games[i].name === arr[j].name) {
              this.games[i].added = true;
            }
          }
        }
      }
    });
  }
  getGames() {
    return this.http.get(window.location.href + 'assets/data.json')
      .toPromise()
      .then(response => response.json());
  }
}
