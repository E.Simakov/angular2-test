import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'; /* импортируем декоратор @NgModule */
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component'; /* Импортируем компонент AppComponent */
import { TitleConvert } from './exponential-strength.pipe';

@NgModule({               /* декоратор @NgModule для класса */
  declarations: [
    AppComponent,
    TitleConvert           /* Указываем что мы добавляем AppComponent в приложение */
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]  /* Говорим о том что для запуска приложения используется AppComponent */
})
export class AppModule { } /* для того что бы импортировать модуль в другой файл добавляется слово - export */
